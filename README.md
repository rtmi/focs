# FOCS

Learning Golang by working through the
 Foundations of Computer Science
 by [Jeff Ullman](http://infolab.stanford.edu/~ullman/ullman-books.html)

As I was reading the different articles on
 Dijkstra&apos;s algorithm, the instructions
 from the Foundations of Computer Science became more clear
 than the blogs and examples I had been referencing earlier.
 Now I want to learn the algorithms, written as Golang.



## Credits

Foundations of Computer Science
 by [Jeff Ullman](http://infolab.stanford.edu/~ullman/ullman-books.html)

