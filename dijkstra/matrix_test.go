package dijkstra

import (
	"testing"
)

func TestMatrix(t *testing.T) {

	m := newMatrixData()

	var dist float64

	dist = m.Edge(0, 1)
	if dist != 1 {
		t.Errorf("Expected 1, rcvd %v", dist)
	}

	dist = m.Edge(0, 2)
	if dist != 2 {
		t.Errorf("Expected 2, rcvd %v", dist)
	}

	dist = m.Edge(1, 4)
	if dist != 3 {
		t.Errorf("Expected 3, rcvd %v", dist)
	}

	dist = m.Edge(2, 4)
	if dist != 4 {
		t.Errorf("Expected 4, rcvd %v", dist)
	}

	dist = m.Edge(4, 5)
	if dist != 5 {
		t.Errorf("Expected 5, rcvd %v", dist)
	}

	dist = m.Edge(3, 0)
	if dist != 6 {
		t.Errorf("Expected 6, rcvd %v", dist)
	}
}

func newMatrixData() *matrix {
	m := &matrix{}
	m.Extend(6)

	m.SetEdge(0, 1, 1)
	m.SetEdge(0, 2, 2)
	m.SetEdge(1, 4, 3)
	m.SetEdge(2, 4, 4)
	m.SetEdge(4, 5, 5)
	m.SetEdge(3, 0, 6)

	return m
}
