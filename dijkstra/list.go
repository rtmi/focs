package dijkstra

import (
	"fmt"
	"strings"
)

// Adjacency list
type list struct {
	value []struct {
		////node node
		adj []cell
	}
}

// SetEdge makes the distance of edge u,v
func (l *list) SetEdge(u, v int, dist float64) {
	// todo neighborName unique constraint

	cel1 := cell{neighborName: v, edgeLabel: dist}
	l.value[u].adj = append(l.value[u].adj, cel1)

	cel2 := cell{neighborName: u, edgeLabel: dist}
	l.value[v].adj = append(l.value[v].adj, cel2)
}

// Edge is the distance of edge u,v
func (l *list) Edge(u, v int) float64 {
	successors := l.value[u].adj

	for _, cel := range successors {
		if v == cel.neighborName {
			return cel.edgeLabel
		}
	}

	// No edge connects u to v
	return 0
}

// Len is the size of the node set
func (l *list) Len() int {
	return len(l.value)
}

// Successors are the adjacent node names and labels
func (l *list) Successors(u int) []cell {
	return l.value[u].adj
}

func (l *list) Extend(max int) {
	if l.Len() == 0 {
		l.value = make([]struct {
			////node node
			adj []cell
		},
			max)
	}
	//else TODO
}

func (l *list) String() (s string) {
	var b strings.Builder

	for k, v := range l.value {
		fmt.Fprintf(&b, "%d)  => %v\n", k, v.adj)
	}

	s = b.String()
	return
}
