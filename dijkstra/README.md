# Dijkstra&apos;s algorithm

Dijkstra&apos;s algorithm will find the minimum distance from one
 given node, a.k.a. source node, to all nodes of the graph.

## How does it work?
We handle a graph represented by an adjacency matrix, or adjacency list.

As an example, this is the Chapter 9 graph of Oahu cities.
 This is our reference during the test creation, and verification.
![Fig. 9.10](diagram/oahu.png)

Keeping this graph in mind, the steps can be described as:

1. Initialize each city distance, e.g. `dist(Laie)`, to **infinity**.
   Except make the source node&apos; distance zero, `dist(Honolulu) = 0`
2. Until all cities are settled, choose one with the smallest
   distance, `Honolulu`. Calculate the path distance for each of its unsettled
   neighbors, e.g. `dist(Honolulu) + edge(Honolulu, Kaneohe)`.
   When the sum is smaller than neighbor&apos;s distance, `dist(Kaneohe)`,
   replace the old value,
   `dist(Kaneohe) = dist(Honolulu) + edge(Honolulu, Kaneohe)`.
3. Move the city, `Honolulu`, to the settled set.


Specifying Honolulu as the source node, discovery of the shortest distance
 will progress like:

| city	| name | loop1 | loop2 | loop3 | ... |
| ------------- | ----- | ----- | ----- | ----- | --- |
| Laie		| 0	| INF	| 35	| 35	| ? |
| Kaneohe	| 1	| 11	| 11	| 11	| 11 |
| Honolulu	| 2	| 0	| 0	| 0	| 0 |
| Pearl		| 3	| 13	| 13	| 13	| 13 |
| Maili		| 4	| INF	| INF	| 33	| ? | 
| Wahiawa	| 5	| INF	| INF	| 25	| 25 |


## Running Time
&Omicron;(m*log n)


## Credits

Hacktoberfest issue call to add structures
 [ektagarg/go-ds](https://github.com/ektagarg/go-ds)

Data structures in Golang
 by [Flavio Copes](https://flaviocopes.com/golang-data-structures/)

Chapter 9 from [Foundations of Computer Science
 by Jeff Ullman](http://infolab.stanford.edu/~ullman/focs.html)

Hacktoberfest
 by [DigitalOcean and DEV](https://blog.digitalocean.com/hacktoberfest-is-back-for-year-six/)


## Hacktoberfest

Originally meant to do this as part of Hacktoberfest, but
 another contributor already made a version of the algorithm.
 As I was reading the different articles, the instructions
 from the Foundations of Computer Science became more clear
 than the blogs and examples I had been referencing earlier.
 So I hope to continue going through the FOCS text book with
 the Golang lense, and learn how the algorithms look as
 Golang code.

