package dijkstra

import (
	"math"
	"sort"
)

// Dijkstra's shortest path main data structures
type dsp struct {
	graph    Graph
	potNodes []int
	value    []node
}

func Dijkstra(g Graph, source int) dsp {
	var u, v int
	var sum float64

	d := dsp{}
	d.initialize(g, source)

	// Last divides the POT list so that indices greater than last
	// are settled, and excluded from further evaluation

	for last := len(d.value); last > 1; {

		// Choose the node with the smallest distance
		v = d.potNodes[1]

		// Exclude the node from iteration
		d.Swap(1, last)
		last--

		sort.Sort(Unsettled{d, last})

		for _, cel := range d.graph.Successors(v) {
			u = cel.neighborName

			// Path with neighbor u
			sum = d.dist(v) + cel.edgeLabel

			// Compare path distance
			if d.dist(u) > sum {
				// Replace distance with smaller value
				d.value[u].dist = sum

				pot := d.value[u].toPOT

				sort.Sort(UpwardFrom{d, pot})
			}
		}
	}

	return d
}

// swap exchanges entries a and b of partially ordered tree and the trackers toPOT
func (d dsp) Swap(i, j int) {
	// Partially ordered tree is not zero-index

	var temp int

	temp = d.potNodes[j]
	d.potNodes[j] = d.potNodes[i]
	d.potNodes[i] = temp
	d.value[d.potNodes[i]].toPOT = i
	d.value[d.potNodes[j]].toPOT = j
}

// priority to eval partially ordered tree for smallest to rise to the top
func (d *dsp) priority(a int) float64 {
	// The partially ordered tree is dereferenced to locate the
	// underlying node where the calculated distance value
	// is stored

	return d.dist(d.potNodes[a])
}

func (d *dsp) initialize(g Graph, source int) {
	// partially ordered tree starts at index 1
	// graph node starts at zero-index

	max := g.Len()
	d.value = make([]node, max)
	d.graph = g
	d.potNodes = make([]int, max+1)

	for nid := 0; nid < max; nid++ {
		acc := node{
			dist:  math.Inf(0),
			toPOT: nid + 1,
		}
		d.value[nid] = acc
		d.potNodes[nid+1] = nid
	}

	// Start the source node's distance at zero
	d.value[source].dist = 0
	if source != 0 {
		// and move it to be first in POT (which is not zero-index)
		d.Swap(1, source+1)
	}
}

// dist is node u's distance
func (d dsp) dist(u int) float64 {
	// Just more for convenience and
	// simpler to read.
	return d.value[u].dist
}
