package dijkstra

import (
	"fmt"
	"strings"
)

// matrix for making dense graphs using adjacency matrix
type matrix struct {
	adj [][]float64
	//value []node
}

// SetEdge makes the distance of edge u,v
func (m *matrix) SetEdge(u, v int, dist float64) {
	m.adj[u][v] = dist
	m.adj[v][u] = dist
}

// Edge is the distance of edge u,v
func (m *matrix) Edge(u, v int) float64 {
	return m.adj[u][v]
}

// Len is the size of the node set
func (m *matrix) Len() int {
	return len(m.adj)
}

// Successors are u's neighbor node names and labels
func (m *matrix) Successors(u int) []cell {
	s := []cell{}

	for name, label := range m.adj[u] {
		if label != 0 {
			s = append(s, cell{name, label})
		}
	}

	return s
}

func (m *matrix) Extend(max int) {
	// Check that the node set has space as needed

	if m.Len() == 0 {
		////m.value = make([]node, max)
		m.adj = make([][]float64, max)
		for i := 0; i < max; i++ {
			m.adj[i] = make([]float64, max)
		}
		return
	}

	if m.Len() < max {
		gap := max - m.Len()

		// Extend the node set
		////m.value = append(m.value, make([]node, gap)...)

		// Extend the adjacency matrix
		m.adj = append(m.adj, make([][]float64, gap)...)
		for i := 0; i < max; i++ {
			m.adj[i] = append(m.adj[i], make([]float64, gap)...)
		}

	}
}

func (m *matrix) String() (s string) {
	var b strings.Builder

	for row := 0; row < m.Len(); row++ {

		for col := 0; col < m.Len(); col++ {
			fmt.Fprintf(&b, "%v ", m.adj[row][col])
		}

		b.WriteString("\n")
	}

	s = b.String()
	return
}
