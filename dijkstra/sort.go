package dijkstra

// UpwardFrom to mimic bubbleUp
type UpwardFrom struct {
	dsp
	toPOT int
}

// Len tells sort.Interface the size of the node set
func (s UpwardFrom) Len() int {
	// bubble-up from the successor's
	// POT position
	return s.toPOT + 1
}

// Less tells sort.Interface when should item-i be sorted before item-j
func (s UpwardFrom) Less(i, j int) bool {
	// The way we use the sort is to
	// bubble-up the successor node
	// when its distance is calculated
	// and discovered to be smaller

	if (j > 1) && s.priority(i) < s.priority(j) {
		return true
	}
	return false
}

// Unsettled to mimic bubbleDown
type Unsettled struct {
	dsp
	last int
}

// Len tells sort.interface size of node set
func (s Unsettled) Len() int {
	// bubble-down until the bottom of
	// the unsettled set (any lower means
	// we reach the settled set)
	return s.last + 1
}

// Less tells sort.interface the situation to sort i before j
func (s Unsettled) Less(i, j int) bool {
	// POT positions are between 1 and last
	// The way we use the sort is to
	// bubble-down the first node which was
	// swapped from the bottom of the unsettled set.

	if (i > 0) && (i < s.last) && (j > 0) && (j <= s.last) &&
		s.priority(i) < s.priority(j) {

		return true
	}
	return false
}
