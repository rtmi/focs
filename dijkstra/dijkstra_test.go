package dijkstra

import (
	"testing"
)

func TestDijkstraDense(t *testing.T) {

	// Start a dense graph
	graph := newOahuGraph(1)

	// Choose Honolulu for the source node (Fig. 9.40)
	sp := Dijkstra(graph, int(honolulu))

	if sp.dist(2) != 0 {
		t.Errorf("Expected Honolulu-0, got %s-%v", city(2).String(), sp.dist(2))
	}

	if sp.dist(3) != 13 {
		t.Errorf("Expected Pearl-13, got %s-%v", city(3).String(), sp.dist(3))
	}

	if sp.dist(4) != 33 {
		t.Errorf("Expected Maili-33, got %s-%v", city(4).String(), sp.dist(4))
	}

	if sp.dist(5) != 25 {
		t.Errorf("Expected Wahiawa-25, got %s-%v", city(5).String(), sp.dist(5))
	}

	if sp.dist(0) != 35 {
		t.Errorf("Expected Laie-35, got %s-%v", city(0).String(), sp.dist(0))
	}

	if sp.dist(1) != 11 {
		t.Errorf("Expected Kaneohe-11, got %s-%v", city(1).String(), sp.dist(1))
	}
}

func TestDijkstraSparse(t *testing.T) {

	// Start a sparse graph
	graph := newOahuGraph(2)

	// Choose Honolulu for the source node (Fig. 9.40)
	sp := Dijkstra(graph, int(honolulu))

	if sp.dist(2) != 0 {
		t.Errorf("Expected Honolulu-0, got %s-%v", city(2).String(), sp.dist(2))
	}

	if sp.dist(3) != 13 {
		t.Errorf("Expected Pearl-13, got %s-%v", city(3).String(), sp.dist(3))
	}

	if sp.dist(4) != 33 {
		t.Errorf("Expected Maili-33, got %s-%v", city(4).String(), sp.dist(4))
	}

	if sp.dist(5) != 25 {
		t.Errorf("Expected Wahiawa-25, got %s-%v", city(5).String(), sp.dist(5))
	}

	if sp.dist(0) != 35 {
		t.Errorf("Expected Laie-35, got %s-%v", city(0).String(), sp.dist(0))
	}

	if sp.dist(1) != 11 {
		t.Errorf("Expected Kaneohe-11, got %s-%v", city(1).String(), sp.dist(1))
	}
}

// test data helpers for Oahu
type city int

const (
	laie city = iota
	kaneohe
	honolulu
	pearl
	maili
	wahiawa
)

func (c city) String() (s string) {
	switch c {
	case laie:
		s = "Laie"
	case kaneohe:
		s = "Kaneohe"
	case honolulu:
		s = "Honolulu"
	case pearl:
		s = "Pearl City"
	case maili:
		s = "Maili"
	case wahiawa:
		s = "Wahiawa"
	default:
		s = "unknown"
	}
	return
}

// newOahuGraph helper routine to make test data for the graph
func newOahuGraph(d int) Graph {
	g := newGraph(d, 6)

	// Make edges as drawn in Fig. 9.10
	g.SetEdge(int(laie), int(kaneohe), 24)
	g.SetEdge(int(kaneohe), int(honolulu), 11)
	g.SetEdge(int(honolulu), int(pearl), 13)
	g.SetEdge(int(pearl), int(maili), 20)
	g.SetEdge(int(pearl), int(wahiawa), 12)
	g.SetEdge(int(maili), int(wahiawa), 15)
	g.SetEdge(int(wahiawa), int(laie), 28)

	return g
}

func newGraph(d int, max int) (g Graph) {
	switch d {
	case 1:
		// dense graph
		m := &matrix{}
		m.Extend(max)
		g = m

	case 2:
		// sparse graph
		l := &list{}
		l.Extend(max)
		g = l

	default:
		panic("Unknown graph density type")
	}

	return
}
