package dijkstra

type Graph interface {
	// SetEdge changes the distance for edge u,v
	SetEdge(u, v int, dist float64)

	// Edge is the distance for edge u,v
	Edge(u, v int) float64

	// Len is the size of the node set
	Len() int

	// Successors are node u's neighbor names and labels
	Successors(u int) []cell
}

// node holds its shortest path distance
type node struct {
	dist float64

	toPOT int
}

// cell is a neighbor-edge
type cell struct {
	neighborName int
	edgeLabel    float64
}
