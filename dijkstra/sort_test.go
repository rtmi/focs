package dijkstra

import (
	"sort"
	"testing"
)

func TestUpwardFrom(t *testing.T) {
	// Start a dense graph
	a := newSortData(1)

	// Verify the initial state of POT
	if a.potNodes[1] != int(honolulu) {
		t.Error("Expect partially ordered tree node-1 to point at honolulu")
	}
	if a.potNodes[int(honolulu)+1] != int(laie) {
		t.Error("Expect partially ordered tree node-3 to point at laie")
	}
	if a.priority(1) != 0 {
		t.Error("Expect honolulu distance to be zero")
	}

	// make pearl's distance be 13, verify it is sorted upward
	a.value[int(pearl)].dist = 13
	successorPOT := a.value[int(pearl)].toPOT
	sort.Sort(UpwardFrom{a, successorPOT})

	resultPearl := a.value[int(pearl)].toPOT
	if resultPearl != 2 {
		t.Errorf("Expected pearl to move to POT node-2, got %d", resultPearl)
	}
	resultHonolulu := a.value[int(honolulu)].toPOT
	if resultHonolulu != 1 {
		t.Errorf("Expected honolulu to stay POT node-1, got %d", resultHonolulu)
	}
}

func TestUnsettled(t *testing.T) {
	// Start a dense graph
	a := newSortData(1)

	// Verify the initial state of POT
	if a.potNodes[1] != int(honolulu) {
		t.Error("Expect partially ordered tree node-1 to point at honolulu")
	}
	if a.potNodes[int(honolulu)+1] != int(laie) {
		t.Error("Expect partially ordered tree node-3 to point at laie")
	}
	if a.priority(1) != 0 {
		t.Error("Expect honolulu distance to be zero")
	}

	// make pearl's distance be 13
	a.value[int(pearl)].dist = 13
	// Move honolulu into settled set
	last := len(a.value)
	a.Swap(1, last)
	last--

	sort.Sort(Unsettled{a, last})

	resultPearl := a.value[int(pearl)].toPOT
	if resultPearl != 1 {
		t.Errorf("Expected pearl to move to POT node-1, got %d", resultPearl)
	}
	resultHonolulu := a.value[int(honolulu)].toPOT
	if resultHonolulu != 6 {
		t.Errorf("Expected honolulu to stay settled, got %d", resultHonolulu)
	}
}

func newSortData(d int) dsp {
	// Use the graph from Fig 9.10
	g := newOahuGraph(d)

	a := dsp{}
	a.initialize(g, int(honolulu))

	return a
}
